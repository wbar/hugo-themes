+++
title = "{{ replace .Name "-" " " | title }}"
date = {{ .Date }}
draft = true

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

## Prompt complet

_Mettre le prompt complet ici_

## Réponse

Contenu de l'article

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

